﻿using ArrayCalculations.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace ArrayCalculations.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ArrayCalcController : ControllerBase
    {
        private readonly IProductService _productService;
        private readonly ILogger<ArrayCalcController> _logger;

        public ArrayCalcController(IProductService productService, ILogger<ArrayCalcController> logger)
        {
            _productService = productService;
            _logger = logger;
        }



        // GET api/reverse
        [HttpGet]
        public async Task<IActionResult> Reverse([FromQuery] int[] productIds)
        {
            _logger.LogInformation($"Reverse api called");
            var result = await _productService.ReverseArray(productIds);
            if (result.IsSuccess)
            {
                //success => return ok 
                _logger.LogError($"Reverse api was successfully executed");
                return Ok(result);
            }
            else
            {
                //error condition => bad request
                _logger.LogError($"Reverse api ended in Bad Request. Details: {result.ErrorMessage}");
                return BadRequest(result);
            }
        }

        // GET api/deletepart
        [HttpGet]
        public async Task<ObjectResult> DeletePart([FromQuery] int position, [FromQuery] int[] productIds)
        {
            _logger.LogInformation($"DeletePart api called");
            var result = await _productService.DeleteElement(productIds, position);
            if (result.IsSuccess)
            {
                //success => return ok
                _logger.LogError($"DeletePart api was successfully executed");
                return Ok(result);
            }
            else
            {
                //error condition => bad request
                _logger.LogError($"DeletePart api ended in Bad Request. Details: {result.ErrorMessage}");
                return BadRequest(result);
            }

        }


    }
}