﻿using System.Threading.Tasks;

namespace ArrayCalculations.Services
{
    public class ProductService : IProductService
    {
        /// <summary>
        /// Deletes an element from the passed array from the specified position
        /// </summary>
        /// <param name="array">Array from which the element is to be deleted</param>
        /// <param name="positionToDelete">position from where the element is to be deleted starting from 1</param>
        /// <returns> <see cref="ProductServiceResult"/></returns>
        public Task<ProductServiceResult> DeleteElement(int[] array, int positionToDelete)
        {
            Task<ProductServiceResult> task = Task<ProductServiceResult>.Run(() =>
            {
                ProductServiceResult result = new ProductServiceResult();
                if (positionToDelete < 1)
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "The position should be greater than zero";
                    return result;
                }
                if (positionToDelete > array.Length)
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "The position is higher than the number of elements";
                    return result;
                }
                if (array.Length == 0)
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "The array of productIds cannot be empty";
                    return result;
                }

                int[] newArray = new int[array.Length - 1];
                int newIndex = 0;
                for (int oldIndex = 0; oldIndex < array.Length; oldIndex++)
                {
                    if (oldIndex == positionToDelete - 1)
                    {
                        continue;
                    }
                    newArray[newIndex++] = array[oldIndex];
                }
                result.IsSuccess = true;
                result.Array = newArray;
                return result;
            });
            return task;
        }

        /// <summary>
        /// Reverses the sequnece of the elements from the passed array 
        /// </summary>
        /// <param name="array">array whose elements are to be reversed</param>
        /// <returns> <see cref="ProductServiceResult"/> /returns>
        public Task<ProductServiceResult> ReverseArray(int[] array)
        {
            Task<ProductServiceResult> task = Task<ProductServiceResult>.Run(() =>
            {
                ProductServiceResult result = new ProductServiceResult();
                if (array.Length == 0)
                {
                    result.IsSuccess = false;
                    result.ErrorMessage = "The array of productIds cannot be empty";
                    return result;
                }
                for (int i = 0; i < array.Length / 2; i++)
                {
                    var temp = array[i];
                    array[i] = array[array.Length - i - 1];
                    array[array.Length - i - 1] = temp;
                }
                result.IsSuccess = true;
                result.Array = array;
                return result;
            });
            return task;
        }
    }
}
