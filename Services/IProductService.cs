﻿using System.Threading.Tasks;

namespace ArrayCalculations.Services
{
    public interface IProductService
    {
        /// <summary>
        /// Reverses the sequnece of the elements from the passed array 
        /// </summary>
        /// <param name="array">array whose elements are to be reversed</param>
        /// <returns> <see cref="ProductServiceResult"/> </returns>
        Task<ProductServiceResult> ReverseArray(int[] array);

        /// <summary>
        /// Deletes an element from the passed array from the specified position
        /// </summary>
        /// <param name="array">Array from which the element is to be deleted</param>
        /// <param name="positionToDelete">position from where the element is to be deleted starting from 1</param>
        /// <returns> <see cref="ProductServiceResult"/> </returns>
        Task<ProductServiceResult> DeleteElement(int[] array, int positionToDelete);

    }

    public class ProductServiceResult
    {
        /// <summary>
        /// True if operation is success else false
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// holds the error message in case of errors
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// It holds the array
        /// </summary>
        public int[] Array { get; set; }

    }
}
