This project is developed using ASP.Net Core Web API.
It has two endpoints as described below

1. Reverse Array - /api/arraycalc/reverse
    Query Params 
	   productIds - array of product ids (required, integer)
	Output - Array in reverse order
    Example - /api/arraycalc/reverse?productIds=1&productIds=2&productIds=3&productIds=4&productIds=5

2. Delete Part - /api/arraycalc/deletepart
    Query Params
	   productIds - int array of product ids (required, integer)
	   position - position to delete the array (number greater than zero); position 1 is the start of the array
	Output - Array with the deleted part
    Example - /api/arraycalc/deletepart?position=3&productIds=1&productIds=2&productIds=3&productIds=4&productIds=5
